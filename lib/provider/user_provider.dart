import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ceiba_app/utils/utils.dart' as utils;
import 'package:ceiba_app/utils/dblite.dart' as dblite;

class UserProvider {
  Future<dynamic> getUsers() async {
    await dblite.deleteLocal();
    String ruta = utils.url();
    late dynamic response;
    List list = [];
    try {
      await http.get(Uri.parse(ruta + '/users')).then((value) {
        response = json.decode(value.body);
      });
    } catch (e) {
      return List.empty();
    }
    if (response != {}) {
      response.forEach((val) {
        insertUsers(val);
        list.add(val);
      });
      return list;
    } else {
      return List.empty();
    }
  }

  Future<dynamic> getPosts(id) async {
    String ruta = utils.url();
    late dynamic response;
    List list = [];
    try {
      await http.get(Uri.parse(ruta + '/posts?userId=$id')).then((value) {
        response = json.decode(value.body);
      });
    } catch (e) {
      return List.empty();
    }
    if (response != {}) {
      response.forEach((val) {
        list.add(val);
      });
      return list;
    } else {
      return List.empty();
    }
  }

  insertUsers(data) async {
    await dblite.createCustomer(data['id'], data['name'], data['username'],
        data['email'], data['phone'], data['website']);
    return true;
  }
}
