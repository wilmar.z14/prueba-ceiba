import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

pathDatabase() async {
  String databasesPath = await getDatabasesPath();
  String dbPath = join(databasesPath, 'Localdatabase.db');
  return dbPath;
}

void localUsersDb(Database database, int version) async {
  await database.execute("CREATE TABLE IF NOT EXISTS Users ("
      "id INTEGER PRIMARY KEY,"
      "name TEXT,"
      "username TEXT,"
      "email TEXT,"
      "phone TEXT,"
      "website TEXT"
      ")");
}

Future<List> getUsers() async {
  var database = await openDatabase(await pathDatabase(),
      version: 1, onCreate: localUsersDb);
  var result = await database.rawQuery('SELECT * FROM Users');
  return result.toList();
}

createCustomer(int id, String name, String username, String email, String phone,
    String website) async {
  String databasesPath = await getDatabasesPath();
  String dbPath = join(databasesPath, 'Localdatabase.db');

  var database = await openDatabase(dbPath, version: 1, onCreate: localUsersDb);
  var result = await database
      .rawInsert("INSERT INTO Users (id, name, username, email, phone, website)"
          " VALUES ('$id','$name','$username','$email','$phone','$website')");
  return result;
}

deleteLocal() async {
  String databasesPath = await getDatabasesPath();
  String dbPath = join(databasesPath, 'Localdatabase.db');

  var database = await openDatabase(dbPath, version: 1, onCreate: localUsersDb);
  var result = await database.rawInsert("DELETE FROM Users");
  return result;
}
