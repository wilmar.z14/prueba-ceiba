import 'package:ceiba_app/provider/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:ceiba_app/utils/dblite.dart' as dblite;

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<dynamic> users = [];
  late Future _future;
  List unfilterData = List.empty();
  String message = 'Cargando Datos';
  TextEditingController searchController = TextEditingController();

  final userProviders = UserProvider();

  @override
  void initState() {
    super.initState();
    _future = getUser(r: 2);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Prueba CEIBA Wilmar Suarez'),
          backgroundColor: Colors.green,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  searchData(value);
                },
                controller: searchController,
                decoration: InputDecoration(
                  hintText: 'Buscar Usuario',
                  contentPadding:
                      const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(32.0)),
                ),
              ),
            ),
            Expanded(child: body(context)),
          ],
        ));
  }

  Future<dynamic> saveUserLocal() async {
    return await userProviders.getUsers().then((value) {
      _future = getUser(r: 2);
    });
  }

  Future getUser({r}) async {
    if (r == 0) {
      await dblite.deleteLocal();
    }
    List response = await dblite.getUsers();
    setState(() {
      users = response;
      unfilterData = response;
    });
    return response;
  }

  Widget body(BuildContext conetxt) {
    return FutureBuilder(
        future: _future,
        builder: (context, snapshot) {
          if (snapshot.data.toString() == "[]") {
            saveUserLocal();
            return Center(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  message,
                  style: const TextStyle(color: Colors.green),
                ),
                const Divider(
                  color: Colors.transparent,
                ),
                const CircularProgressIndicator(),
              ],
            ));
          } else {
            final lngh = (users.isNotEmpty) ? users.length : 1;
            return ListView.builder(
              itemCount: lngh,
              itemBuilder: (BuildContext context, index) {
                if (users.isNotEmpty) {
                  return createWidget(context, users[index]);
                } else {
                  return const Center(child: Text('List is Empty'));
                }
              },
            );
          }
        });
  }

  Widget createWidget(context, data) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          ListTile(
            title: Text(
              data['name'],
              style: const TextStyle(fontSize: 18.0, color: Colors.green),
            ),
            subtitle: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.only(right: 2.0),
                  child: Icon(
                    Icons.phone,
                    color: Colors.green,
                  ),
                ),
                Text(
                  data['phone'],
                  style: const TextStyle(color: Colors.black),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.only(right: 2.0),
                  child: Icon(
                    Icons.email,
                    color: Colors.green,
                  ),
                ),
                Text(
                  data['email'],
                  style: const TextStyle(color: Colors.black),
                ),
              ],
            ),
          ),
          ButtonBar(
            alignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () {
                  viewPost(data);
                },
                child: const Text(
                  'VER PUBLICACIONES',
                  style: TextStyle(fontSize: 15.0),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  viewPost(data) {
    Navigator.pushNamed(context, 'post', arguments: data);
  }

  searchData(str) {
    str = str.toString().toUpperCase();
    var strExist = str.length > 0 ? true : false;

    if (strExist) {
      var filterData = [];
      for (var i = 0; i < unfilterData.length; i++) {
        String word = unfilterData[i]['name'].toString().toUpperCase();
        if (word.contains(str)) {
          filterData.add(unfilterData[i]);
        }
      }

      setState(() {
        users = filterData;
      });
    } else {
      setState(() {
        users = unfilterData;
      });
    }
  }
}
