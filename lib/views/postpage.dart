import 'package:ceiba_app/provider/user_provider.dart';
import 'package:flutter/material.dart';

class PostPage extends StatefulWidget {
  const PostPage({Key? key}) : super(key: key);

  @override
  _PostPageState createState() => _PostPageState();
}

class _PostPageState extends State<PostPage> {
  String message = 'Cargando Post';
  final userProviders = UserProvider();
  List posts = [];

  @override
  Widget build(BuildContext context) {
    final Map data = ModalRoute.of(context)!.settings.arguments as Map;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: const Center(child: Text('Posts Realizados por el usuario')),
      ),
      body: Column(
        children: [
          Stack(
            children: <Widget>[
              Card(
                margin: const EdgeInsets.only(top: 20.0),
                child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 45.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Text(data['name'].toUpperCase()),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5.0),
                            child: Text(data['email'].toUpperCase()),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(top: 5.0, bottom: 10.0),
                            child: Text(data['phone'].toUpperCase()),
                          )
                        ],
                      ),
                    )),
              ),
              Positioned(
                top: .0,
                left: .0,
                right: .0,
                child: Center(
                  child: CircleAvatar(
                    backgroundColor: Colors.green,
                    radius: 30.0,
                    child: Text(data['name'].substring(0, 1).toUpperCase()),
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: FutureBuilder(
                future: getPost(data['id']),
                builder: (context, snapshot) {
                  if (snapshot.data == null) {
                    return Center(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          message,
                          style: const TextStyle(color: Colors.green),
                        ),
                        const Divider(
                          color: Colors.transparent,
                        ),
                        const CircularProgressIndicator(),
                      ],
                    ));
                  } else {
                    final List data = snapshot.data as List;
                    return ListView.builder(
                      itemCount: data.length,
                      itemBuilder: (BuildContext context, index) {
                        if (posts.isEmpty) {
                          return const Text('Not data');
                        } else {
                          return createWidgetPost(context, data[index]);
                        }
                      },
                    );
                  }
                }),
          ),
        ],
      ),
    );
  }

  Widget createWidgetPost(context, data) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Card(
        elevation: 10.0,
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: ListTile(
            title: Text(data['title'].toUpperCase()),
            subtitle: Text(data['body']),
          ),
        ),
      ),
    );
  }

  Future getPost(id) async {
    final response = await userProviders.getPosts(id);
    posts = response;
    return response;
  }
}
