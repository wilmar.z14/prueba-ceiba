import 'package:ceiba_app/views/homepage.dart';
import 'package:ceiba_app/views/postpage.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Prueba CEIBA',
      initialRoute: 'home',
      routes: {
        //routes principales
        'home': (BuildContext context) => const HomePage(),
        'post': (BuildContext context) => const PostPage(),
      },
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
    );
  }
}
