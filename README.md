# ceiba_app

A new Flutter project.

Prueba presentada por : Wilmar Alejandro Suarez Franco

version Flutter: 2.5.3
version Dart: 2.14.4

** A continuacion detallo aspectos a tener en cuenta para ejecutar esta app
Inicialmente ejecutar el comando "flutter pub get" esto para actualizar los paquetes necesarios

** Para probar la funcionalidad del almacenamiento local de datos. En el archivo homepage.dart, en la linea 24 (_future = getUser(r: 2);) cambiar el valor de r a cualquier numero diferente de 2
esto, para eliminar los datos locales e ingresar los del ws

Espero esta prueba cumpla todos los items enviados.
